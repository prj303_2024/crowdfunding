// document.addEventListener('DOMContentLoaded', async () => {
//     try {
//         const response = await fetch('/feedback');
        
//         if (!response.ok) {
//             if (response.status === 404) {
//                 document.getElementById('feedback-entries').innerHTML = '<tr><td colspan="6">No feedback found</td></tr>';
//             } else {
//                 console.error('Failed to fetch feedback data');
//             }
//             return;
//         }
        
//         const feedbacks = await response.json();
//         const feedbackEntries = document.getElementById('feedback-entries');

//         feedbacks.forEach(feedback => {
//             const row = document.createElement('tr');
            
//             const nameCell = document.createElement('td');
//             nameCell.textContent = feedback.name;
//             row.appendChild(nameCell);

//             const emailCell = document.createElement('td');
//             emailCell.textContent = feedback.email;
//             row.appendChild(emailCell);

//             const subjectCell = document.createElement('td');
//             subjectCell.textContent = feedback.subject;
//             row.appendChild(subjectCell);

//             const messageCell = document.createElement('td');
//             messageCell.textContent = feedback.message;
//             row.appendChild(messageCell);

//             const ratingCell = document.createElement('td');
//             ratingCell.textContent = feedback.rating;
//             row.appendChild(ratingCell);

//             const createdAtCell = document.createElement('td');
//             createdAtCell.textContent = new Date(feedback.createdAt).toLocaleString();
//             row.appendChild(createdAtCell);

//             feedbackEntries.appendChild(row);
//         });
//     } catch (error) {
//         console.error('Error fetching feedback data:', error);
//     }
// });
