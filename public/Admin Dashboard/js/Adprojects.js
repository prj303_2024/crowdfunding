document.addEventListener('DOMContentLoaded', async () => {
    const projectsContainer = document.getElementById('projects');
    const loadingAnimation = document.getElementById('loading-animation');

    // Fetch projects from the API
    async function fetchProjects() {
        // Show the loading animation
        loadingAnimation.style.display = 'block';

        try {
            const response = await fetch('/projects/all');
            const projects = await response.json();
            console.log('Fetched projects:', projects); // Debugging
            displayProjects(projects);
        } catch (error) {
            console.error('Error fetching projects:', error);
        } finally {
            // Hide the loading animation
            loadingAnimation.style.display = 'none';
        }
    }

    // Display projects in the DOM
    function displayProjects(projects) {
        console.log('Displaying projects:', projects); // Debugging
        if (projects.length === 0) {
            projectsContainer.innerHTML = '<p>No projects found.</p>';
            return;
        }

        // Sort projects by createdAt in descending order
        projects.sort((a, b) => new Date(b.createdAt) - new Date(a.createdAt));

        const table = document.createElement('table');
        table.classList.add('table', 'table-striped');

        const thead = document.createElement('thead');
        thead.innerHTML = `
          <tr>
              <th>Image</th>
              <th>Title</th>
              <th>Status</th>
              <th>Target Amount</th>
              <th>Created At</th>
              <th>Actions</th>
          </tr>
      `;
        table.appendChild(thead);

        const tbody = document.createElement('tbody');

        projects.forEach(project => {
            console.log('Project:', project); // Debugging

            const row = document.createElement('tr');

            // Convert the buffer to a Base64 string safely
            let imageSrc = '';
            if (project.image && project.image.data) {
                const binary = new Uint8Array(project.image.data).reduce((data, byte) => data + String.fromCharCode(byte), '');
                imageSrc = `data:image/png;base64,${btoa(binary)}`;
            }

            row.innerHTML = `
              <td><img src="${imageSrc}" class="img-thumbnail"></td>
              <td>${project.title}</td>
              <td>${project.status}</td>
              <td>${project.targetAmount} XRP</td>
              <td>${new Date(project.createdAt).toLocaleString()}</td>
              <td>
                  <button class="approve-button" onclick="approveProject('${project.projectId}')">Approve</button>
                  <button class="rejected-button" onclick="rejectProject('${project.projectId}')">Reject</button>
                  <button class="view-button" onclick="viewProjectDetails('${project.projectId}')">View More</button>
                  <button class="delete-button" onclick="deleteProject('${project.projectId}')">Delete</button>
              </td>
          `;
            tbody.appendChild(row);
        });

        table.appendChild(tbody);
        projectsContainer.innerHTML = ''; // Clear existing content
        projectsContainer.appendChild(table);
    }

    // Function to handle viewing project details
    window.viewProjectDetails = async (projectId) => {
        const project = await fetchProjectDetails(projectId);
        displayProjectDetails(project);
        $('#projectDetailsModal').modal('show');
    };

    // Fetch project details
    async function fetchProjectDetails(projectId) {
        try {
            const response = await fetch(`/projects/${projectId}`);
            const project = await response.json();
            return project;
        } catch (error) {
            console.error('Error fetching project details:', error);
        }
    }

    // Display project details in the modal
    function displayProjectDetails(project) {
        const projectDetailsContent = document.getElementById('projectDetailsContent');
        projectDetailsContent.innerHTML = `
          <p><strong>Title:</strong> ${project.title}</p>
          <p><strong>Description:</strong> ${project.description}</p>
          <p><strong>Target Amount:</strong> ${project.targetAmount} XRP</p>
          <p><strong>Status:</strong> ${project.status}</p>
          <p><strong>Created At:</strong> ${new Date(project.createdAt).toLocaleString()}</p>
          <p><strong>End Date:</strong> ${new Date(project.endDate).toLocaleString()}</p>
      `;
    }

    // Show loading message
    function showLoadingMessage() {
        document.getElementById('loadingOverlayAdmin').style.display = 'flex';
    }

    // Hide loading message
    function hideLoadingMessage() {
        document.getElementById('loadingOverlayAdmin').style.display = 'none';
    }

    // Function to approve a project
    window.approveProject = async (projectId) => {
        const result = await Swal.fire({
            title: 'Are you sure?',
            text: "You want to approve this project!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, approve it!',
            cancelButtonText: 'No, cancel!'
        });

        if (result.isConfirmed) {
            showLoadingMessage();
            try {
                const response = await fetch(`/projects/${projectId}/approve`, {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                });
                if (response.ok) {
                    hideLoadingMessage(); // Ensure loading disappears before showing modal
                    showSuccessMessage('Project approved successfully');
                    await fetchProjects(); // Refresh projects list
                } else {
                    const errorData = await response.json();
                    hideLoadingMessage(); // Ensure loading disappears before showing modal
                    showErrorMessage(`Error approving project: ${errorData.error}`);
                    console.error('Error approving project:', errorData);
                }
            } catch (error) {
                hideLoadingMessage(); // Ensure loading disappears before showing modal
                showErrorMessage('Error approving project');
                console.error('Error approving project:', error);
            }
        }
    };

    // Function to delete a project
    // Function to delete a project
    window.deleteProject = async (projectId) => {
        const result = await Swal.fire({
            title: 'Are you sure?',
            text: "You want to delete this project!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!'
        });

        if (result.isConfirmed) {
            showLoadingMessage();
            try {
                const response = await fetch(`/projects/${projectId}`, {
                    method: 'DELETE',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                });
                if (response.ok) {
                    hideLoadingMessage(); // Ensure loading disappears before showing SweetAlert
                    showSuccessMessage('Project deleted successfully');
                    await fetchProjects(); // Refresh projects list
                } else {
                    const errorData = await response.json();
                    hideLoadingMessage(); // Ensure loading disappears before showing SweetAlert
                    showErrorMessage(`Error deleting project: ${errorData.error}`);
                    console.error('Error deleting project:', errorData);
                }
            } catch (error) {
                hideLoadingMessage(); // Ensure loading disappears before showing SweetAlert
                showErrorMessage('Error deleting project');
                console.error('Error deleting project:', error);
            }
        }
    };


    // Function to reject a project
    window.rejectProject = async (projectId) => {
        const result = await Swal.fire({
            title: 'Are you sure?',
            text: "You want to reject this project!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, reject it!',
            cancelButtonText: 'No, cancel!'
        });

        if (result.isConfirmed) {
            showLoadingMessage();
            try {
                const response = await fetch(`/projects/${projectId}/reject`, {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                });
                if (response.ok) {
                    hideLoadingMessage(); // Ensure loading disappears before showing modal
                    showSuccessMessage('Project rejected successfully');
                    await fetchProjects(); // Refresh projects list
                } else {
                    const errorData = await response.json();
                    hideLoadingMessage(); // Ensure loading disappears before showing modal
                    showErrorMessage(`Error rejecting project: ${errorData.error}`);
                    console.error('Error rejecting project:', errorData);
                }
            } catch (error) {
                hideLoadingMessage(); // Ensure loading disappears before showing modal
                showErrorMessage('Error rejecting project');
                console.error('Error rejecting project:', error);
            }
        }
    };


    function showSuccessMessage(message) {
        hideLoadingMessage(); // Ensure loading disappears
        Swal.fire({
            title: 'Success!',
            text: message,
            icon: 'success',
            confirmButtonText: 'OK'
        });
    }

    // Function to show error message using SweetAlert
    function showErrorMessage(message) {
        hideLoadingMessage(); // Ensure loading disappears
        Swal.fire({
            title: 'Error!',
            text: message,
            icon: 'error',
            confirmButtonText: 'OK'
        });
    }

    // Initial fetch of projects
    await fetchProjects();
});
