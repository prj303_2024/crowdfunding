const hamBurger = document.querySelector(".toggle-btn");

hamBurger.addEventListener("click", function () {
  document.querySelector("#sidebar").classList.toggle("expand");
});

fetch('/user-stats')
  .then(response => response.json())
  .then(stats => {
    // Update total project created
    document.getElementById('totalProjectCreatedValue').textContent = stats.totalCreators || 'N/A';

    // Update total users
    document.getElementById('totalUserValue').textContent = stats.totalUsers || 'N/A';

    // Update total project invested
    document.getElementById('totalProjectInvestedValue').textContent = stats.totalInvestors || 'N/A';
  })
  .catch(error => {
    console.error('Error fetching user stats:', error);
    // Handle error
  });

  
  