document.addEventListener('DOMContentLoaded', () => {
    const signUpForm = document.querySelector('#signUpModal form');
    const errorMessage = document.querySelector('#signUpModal .error-message');
    const successMessage = document.querySelector('#signUpModal .success-message');
    const signUpModalElement = document.getElementById('signUpModal');
    const loginModalElement = document.getElementById('loginModal');

    signUpForm.addEventListener('submit', async (event) => {
        event.preventDefault();

        const formData = new FormData(signUpForm);
        const userData = {
            name: formData.get('name'),
            email: formData.get('email'),
            userType: formData.get('userType'),
            walletAddress: formData.get('walletAddress'),
            // walletSecret: formData.get('walletSecret'),
            password: formData.get('password'),
            confirmPassword: formData.get('confirm-password')
        };

        // Perform validations
        const validationErrors = validateForm(userData);
        if (validationErrors.length > 0) {
            showError(validationErrors.join('\n'));
            return;
        }

        try {
            const response = await fetch('/register', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(userData)
            });

            if (response.ok) {
                const result = await response.text();
                showSuccess(result);
            } else {
                const errorText = await response.text();
                showError(errorText);
            }
        } catch (error) {
            console.error('Error:', error);
            showError('Registration failed. Please try again later.');
        }
    });

    function validateForm(userData) {
        const errors = [];

        if (!userData.name) {
            errors.push('Name is required.');
        } else if (!isValidName(userData.name)) {
            errors.push('Name can only contain characters.');
        }

        if (!userData.email) {
            errors.push('Email is required.');
        } else if (!isValidEmail(userData.email)) {
            errors.push('Email is not valid.');
        }

        if (!userData.userType) {
            errors.push('User type is required.');
        }

        if (!userData.walletAddress) {
            errors.push('Wallet address is required.');
        }

        // if (!userData.walletSecret) {
        //     errors.push('Wallet secret is required.');
        // }

        if (!userData.password) {
            errors.push('Password is required.');
        } else if (!isValidPassword(userData.password)) {
            errors.push('Password must be at least 8 characters long and contain at least one uppercase letter and one number.');
        }

        if (!userData.confirmPassword) {
            errors.push('Confirm password is required.');
        } else if (userData.password !== userData.confirmPassword) {
            errors.push('Passwords do not match.');
        }

        return errors;
    }

    function isValidName(name) {
        const namePattern = /^[a-zA-Z]+$/;
        return namePattern.test(name);
    }

    function isValidEmail(email) {
        const emailPattern = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
        return emailPattern.test(email);
    }

    function isValidPassword(password) {
        const passwordPattern = /^(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/;
        return passwordPattern.test(password);
    }


    function showError(message) {
        Swal.fire({
            title: 'Error!',
            text: message,
            icon: 'error',
            confirmButtonText: 'Ok'
        });
    }

    function showSuccess(message) {
        Swal.fire({
            title: 'Good job!',
            text: message,
            icon: 'success',
            confirmButtonText: 'Done'
        }).then(() => {
            signUpForm.reset();

            // Hide the sign-up modal
            const signUpModalInstance = bootstrap.Modal.getInstance(signUpModalElement);
            signUpModalInstance.hide();

            // Ensure the modal is fully hidden before showing the login modal
            signUpModalElement.addEventListener('hidden.bs.modal', () => {
                const loginModalInstance = new bootstrap.Modal(loginModalElement);
                loginModalInstance.show();
            }, { once: true });
        });

    }
});