async function fetchProjectDetails(projectId) {

    try {
        const campaignDetails = document.getElementById('campaignDetails');
        const response = await fetch(`/projects/${projectId}`);
        if (!response.ok) {
            throw new Error('Network response was not ok');
        }
        const campaign = await response.json();
        const reader = new FileReader();
        if (campaign.image && campaign.image.data) {
            reader.onload = async function (event) {
                const imageDataUrl = event.target.result;

                const formattedEndDate = new Date(campaign.endDate).toLocaleDateString('en-US', {
                    year: 'numeric',
                    month: 'long',
                    day: 'numeric'
                });

                campaignDetails.innerHTML = `
                    <section class="projectSection row" data-aos="fade-up">
                    <h2 data-aos="fade-up">Details of the Projects</h2>
                    <hr/>
                        <div class="imageClass col-lg-7" data-aos="fade-right">
                            <img src="${imageDataUrl}" alt="Campaign Image">
                        </div>
                        <div class="detailsClass col-lg-5" data-aos="fade-left">
                            <h3> ${campaign.title}</h3>
                            <div class="walletAddress">
                                <p class="heading" style="color: #333;">Creator Wallet Address:</p>
                                <p style="color: #008066;">${campaign.creatorWalletAddress}</p>
                            </div>
                            <div class="walletAddress">
                                <p class="heading" style="color: #333;">Status:</p>
                                <p style="color: #008066;">${campaign.status}</p>
                            </div>              
                            <div class="walletAddress">
                                <p class="heading" style="color: #333;">Target Amount:</p>
                                <p style="color: #008066;">${campaign.targetAmount} XRP</p>
                            </div> 
                            <div class="end-date">
                                <p class="heading" style="color: #333;">End Date:</p>
                                <p style="color: #008066;">${formattedEndDate}</p>
                            </div>
                        
                            <div id="raisedAmount_${campaign.projectId}">
                            <span class="raised-amount-text">Raised Amount: Loading...</span>
                            <div class="progress-bar">
            
                                <div class="progress-bar-fill" style="width: 0%;">0%</div>
            
                            </div>
            
                        </div>
            
                        <button class="view-transactions-btn" data-campaign-id="${campaign.projectId}">View Transactions</button>
            
                        <button class="donate-btn" data-project-id="${campaign.projectId}">Invest</button>
                    </div>
                    <secton class="projectDescription" data-aos="fade-up">
                        <p> ${campaign.description}</p>
                    </section>
                </section>

                `;
                // Attach event listener for viewing transactions
                const viewTransactionsBtn = campaignDetails.querySelector('.view-transactions-btn');
                viewTransactionsBtn.addEventListener('click', () => {
                    const projectId = viewTransactionsBtn.dataset.campaignId;
                    if (projectId) {
                        fetchCampaignTransactions(projectId);
                    } else {
                        console.error('Invalid campaign ID');
                    }
                });

                // Attach event listener for donating
                const donateBtn = campaignDetails.querySelector('.donate-btn');
                donateBtn.addEventListener('click', () => {
                    const projectId = donateBtn.dataset.projectId;
                    if (projectId) {
                        initiateInvestment(projectId);
                    } else {
                        console.error('Invalid project ID');
                    }

                });


                // Fetch raised amount for the campaign

                try {

                    const raisedAmountDiv = campaignDetails.querySelector(`#raisedAmount_${campaign.projectId}`);
                    const progressBarFill = raisedAmountDiv.querySelector('.progress-bar-fill');
                    const raisedAmountText = raisedAmountDiv.querySelector('.raised-amount-text');
                    const raisedAmountResponse = await fetch(`/projects/${campaign.projectId}/raised`);
                    const raisedAmountData = await raisedAmountResponse.json();
                    const raisedAmount = raisedAmountData.totalRaised;
                    const targetAmount = campaign.targetAmount;
                    const raisedPercentage = (raisedAmount / targetAmount) * 100;
                    progressBarFill.style.width = `${raisedPercentage}%`;
                    progressBarFill.textContent = `${raisedPercentage.toFixed(2)}%`;
                    raisedAmountText.innerHTML = `Raised Amount: <span class="value"> ${raisedAmount} XRP</span>`;
                } catch (error) {
                    console.error('Error fetching raised amount for campaign:', error);
                }
            };

            // Convert the ArrayBuffer to Blob
            const blob = new Blob([new Uint8Array(campaign.image.data)], { type: 'image/jpeg' });
            // Read the Blob as a data URL
            reader.readAsDataURL(blob);
        } else {
            console.error('Image data is missing for campaign:', campaign);
        }

    } catch (error) {
        console.error('Error fetching project details:', error);
        const campaignDetails = document.getElementById('campaignDetails');
        campaignDetails.innerHTML = '<p>Error loading project details. Please try again later.</p>'; // Show error message
    }
}

function getProjectIdFromUrl() {
    const urlParams = new URLSearchParams(window.location.search);
    return urlParams.get('projectId');
}

// Fetch project details when the page loads
window.onload = async () => {
    const projectId = getProjectIdFromUrl();
    if (projectId) {
        await fetchProjectDetails(projectId);
    } else {
        const campaignDetails = document.getElementById('campaignDetails');
        campaignDetails.innerHTML = '<p>Invalid project ID.</p>'; // Show error message
    }
};

function openOverlay(id) {
    const overlay = document.getElementById(id);
    overlay.style.display = 'block';
}

// Function to close overlay
function closeOverlay(id) {
    const overlay = document.getElementById(id);
    overlay.style.display = 'none';
}

async function fetchCampaignTransactions(projectId) {
    try {
        const response = await fetch(`/projects/${projectId}/transactions`);
        const transactions = await response.json();
        const transactionList = document.getElementById('transactionList');
        transactionList.innerHTML = '';

        if (transactions.length === 0) {
            const noTransactionsMessage = document.createElement('p');
            noTransactionsMessage.textContent = 'No transactions available.';
            transactionList.appendChild(noTransactionsMessage);
        } else {
            // Create the table and header row
            const table = document.createElement('table');
            table.classList.add('transaction-table');
            const headerRow = document.createElement('tr');
            headerRow.innerHTML = `
                <th>Ledger Index</th>
                <th>Transaction ID</th>
                <th>Sender</th>
                <th>Amount</th>
                <th>Time</th>
            `;
            table.appendChild(headerRow);

            // Reverse the order of transactions to display the latest ones at the top
            transactions.reverse().forEach(transaction => {
                const date = new Date(transaction.timestamp);
                const timeOptions = { hour: '2-digit', minute: '2-digit', second: '2-digit', hour12: true };
                const dateOptions = { year: 'numeric', month: 'long', day: 'numeric', timeZoneName: 'long' };
                const formattedTime = date.toLocaleTimeString('en-US', timeOptions);
                const formattedDate = date.toLocaleDateString('en-US', dateOptions);

                // Shorten the transaction ID, showing only the first 8 and last 8 characters
                const shortenedTransactionId = `${transaction.transactionId.slice(0, 8)}...${transaction.transactionId.slice(-8)}`;

                const row = document.createElement('tr');
                row.innerHTML = `
                    <td>${transaction.ledgerIndex}</td>
                    <td>
                        <span class="short-txid">${shortenedTransactionId}</span>
                        <span class="full-txid d-none">${transaction.transactionId}</span>
                        <button class="toggle-txid-btn" onclick="toggleTransactionId(this)">View Full</button>
                    </td>
                    <td>${transaction.sender}</td>
                    <td>${transaction.amount}</td>
                    <td>${formattedTime} on ${formattedDate}</td>
                `;
                table.appendChild(row);
            });

            transactionList.appendChild(table);
        }
        openOverlay('transactionOverlay'); // Show the overlay
    } catch (error) {
        console.error('Error fetching campaign transactions:', error);
    }
}

function toggleTransactionId(button) {
    const row = button.parentElement;
    const shortTxId = row.querySelector('.short-txid');
    const fullTxId = row.querySelector('.full-txid');

    if (fullTxId.classList.contains('d-none')) {
        // Show full transaction ID
        fullTxId.classList.remove('d-none');
        shortTxId.classList.add('d-none');
        button.textContent = 'View Short';
    } else {
        // Show short transaction ID
        fullTxId.classList.add('d-none');
        shortTxId.classList.remove('d-none');
        button.textContent = 'View Full';
    }
}


async function initiateInvestment(projectId) {
    // Show the investment modal
    document.getElementById('investmentModal').style.display = 'block';

    const modal = document.getElementById('investmentModal');
    const form = document.getElementById('investmentForm');
    const walletSecretInput = document.getElementById('walletSecret');
    const amountInput = document.getElementById('amount');
    const loadingIndicator = document.getElementById('loadingTransactionOverlay');

    // Add event listener for form submission
    form.addEventListener('submit', async (event) => {
        event.preventDefault(); // Prevent form from submitting by default

        // Show the confirmation dialog immediately after clicking "Invest"
        const confirmation = await Swal.fire({
            title: 'Are you sure?',
            text: "Do you want to invest in this project?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, invest it!',
            cancelButtonText: 'Cancel',
        });

        // If the user cancels, do not proceed
        if (!confirmation.isConfirmed) {
            return;
        }

        // Show the loading indicator immediately after user confirms
        loadingIndicator.style.display = 'flex';

        // Proceed only if user confirms
        const walletSecret = walletSecretInput.value;
        const amount = parseFloat(amountInput.value); // Ensure the amount is a number

        // Validate wallet secret and amount
        if (!walletSecret || isNaN(amount) || amount <= 0) {
            loadingIndicator.style.display = 'none'; // Hide loading indicator if validation fails
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Please provide a valid wallet secret and investment amount.',
            });
            return;
        }

        // Fetch the current total raised amount for the project
        let totalRaised = 0;
        try {
            const response = await fetch(`/projects/${projectId}/transactions`);
            const transactions = await response.json();
            totalRaised = transactions.reduce((total, transaction) => total + transaction.amount, 0);
        } catch (error) {
            console.error('Error fetching campaign transactions:', error);
            loadingIndicator.style.display = 'none'; // Hide loading indicator on error
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Could not fetch current total raised amount. Please try again later.',
            });
            return;
        }

        // Fetch the project details to get the target amount
        let project;
        try {
            const response = await fetch(`/projects/${projectId}`);
            project = await response.json();
        } catch (error) {
            console.error('Error fetching project details:', error);
            loadingIndicator.style.display = 'none'; // Hide loading indicator on error
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Could not fetch project details. Please try again later.',
            });
            return;
        }

        const remainingAmount = project.targetAmount - totalRaised;

        // Check if the investment exceeds the remaining amount
        if (amount > remainingAmount) {
            loadingIndicator.style.display = 'none'; // Hide loading indicator on validation error
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: `You can only invest up to ${remainingAmount} XRP, as this project has reached its target amount.`,
            });
            return;
        }

        try {
            // Send the investment request
            const response = await fetch('/invest', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ walletSecret, amount, projectId }),
            });

            const data = await response.json();

            if (response.ok) {
                // Immediately update the raised amount and progress bar
                await updateRaisedAmountAndProgress(projectId, amount); // Pass the amount invested
                loadingIndicator.style.display = 'none'; // Hide the loading overlay
                modal.style.display = 'none'; // Close the modal

                // Show success message
                Swal.fire({
                    icon: 'success',
                    title: 'Success!',
                    text: 'Investment successful!',
                });
            } else {
                // Handle investment failure
                throw new Error(data.error);
            }
        } catch (error) {
            console.error('Error during investment:', error);
            // Show error message and hide loading indicator
            loadingIndicator.style.display = 'none';
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'An error occurred: ' + error.message,
            })
        }
    });
}




async function updateRaisedAmountAndProgress(projectId, investmentAmount) {
    try {
        const raisedAmountDiv = document.getElementById(`raisedAmount_${projectId}`);
        const progressBarFill = raisedAmountDiv.querySelector('.progress-bar-fill');
        const raisedAmountText = raisedAmountDiv.querySelector('.raised-amount-text');

        // Fetch the updated raised amount
        const raisedAmountResponse = await fetch(`/projects/${projectId}/raised`);
        const raisedAmountData = await raisedAmountResponse.json();

        const raisedAmount = parseFloat(raisedAmountData.totalRaised) || 0; // Default to 0 if NaN
        const targetAmountResponse = await fetch(`/projects/${projectId}`);
        const targetAmountData = await targetAmountResponse.json();
        const targetAmount = parseFloat(targetAmountData.targetAmount) || 1; // Default to 1 to avoid division by zero

        // Calculate the raised percentage
        const raisedPercentage = (raisedAmount / targetAmount) * 100;

        // Ensure max 100%
        progressBarFill.style.width = `${Math.min(raisedPercentage, 100)}%`;
        progressBarFill.textContent = `${Math.min(raisedPercentage, 100).toFixed(2)}%`;
        raisedAmountText.textContent = `Raised Amount: ${raisedAmount}`;

    } catch (error) {
        console.error('Error updating raised amount and progress bar:', error);
    }
}