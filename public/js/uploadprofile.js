document.addEventListener("DOMContentLoaded", async function () {
    try {
        const response = await fetch('/profile', {
            credentials: 'include' // Include cookies in the request
        });

        if (!response.ok) {
            throw new Error('Failed to fetch user profile');
        }

        const data = await response.json();
        console.log('Profile data:', data); // Debug: Log the data

        if (data) {
            fillForm(data);
        } else {
            throw new Error('No profile data found');
        }
    } catch (error) {
        console.error('Error fetching user profile:', error);
        displayError(error.message || 'Failed to fetch user profile');
    }
});

function fillForm(profile) {
    document.getElementById('name').value = profile.name || '';
    document.getElementById('email').value = profile.email || '';
}

function displayError(message) {
    const errorContainer = document.getElementById('error');
    errorContainer.textContent = message;
    errorContainer.style.display = 'block';
}

// Upload profile picture
document.getElementById('profile-picture-form').addEventListener('submit', async function (event) {
    event.preventDefault();

    const formData = new FormData();
    formData.append('profilePicture', document.getElementById('profilePicture').files[0]);

    try {
        const response = await fetch('/profile-picture', {
            method: 'PUT',
            body: formData,
            credentials: 'include'
        });

        const result = await response.text();
        const uploadResult = document.getElementById('upload-result');

        if (response.ok) {
            uploadResult.textContent = result;
            uploadResult.className = 'success';
            uploadResult.style.display = 'block';

            // Reload profile information
            const profileResponse = await fetch('/profile', { credentials: 'include' });
            const profileData = await profileResponse.json();
            if (profileResponse.ok) {
                console.log('Profile Uploaded');
            } else {
                throw new Error(profileData.message || 'Failed to fetch updated profile');
            }
        } else {
            throw new Error(result);
        }
    } catch (error) {
        console.error('Error uploading profile picture:', error);
        const uploadResult = document.getElementById('upload-result');
        uploadResult.textContent = error.message;
        uploadResult.className = 'error';
        uploadResult.style.display = 'block';
    }
});



document.getElementById('profile-update-form').addEventListener('submit', async function (event) {
    event.preventDefault();

    const name = document.getElementById('name').value;
    const email = document.getElementById('email').value;
    const currentPassword = document.getElementById('currentPassword').value;

    const response = await fetch('/profile', {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        },
        credentials: 'include', // include cookies in the request
        body: JSON.stringify({
            name,
            email,
            currentPassword
        })
    });

    const result = document.getElementById('result');
    const error = document.getElementById('error');

    if (response.ok) {
        result.style.display = 'block';
        error.style.display = 'none';
    } else {
        const errorMessage = await response.text();
        error.textContent = errorMessage;
        error.style.display = 'block';
        result.style.display = 'none';
    }
});