async function fetchProjectDetails(projectId) {
    try {
        const campaignDetails = document.getElementById('campaignDetails');
        const response = await fetch(`/projects/${projectId}`);

        if (!response.ok) {
            throw new Error('Network response was not ok');
        }

        const campaign = await response.json();

        if (campaign.image && campaign.image.data) {
            const reader = new FileReader();

            reader.onload = async function (event) {
                const imageDataUrl = event.target.result;

                const formattedEndDate = new Date(campaign.endDate).toLocaleDateString('en-US', {
                    year: 'numeric',
                    month: 'long',
                    day: 'numeric'
                });

                campaignDetails.innerHTML = `
    <section id='project-cr-details' class="projectSection row" data-aos="fade-up">
        <div class="imageClass col-lg-7" data-aos="fade-right">
            <img src="${imageDataUrl}" alt="Campaign Image">
        </div>
        <div class="detailsClass col-lg-5" data-aos="fade-left">
            <h3>${campaign.title}</h3>
            <div class="walletAddress">
                <p class="heading" style="color: #333;">Creator Wallet Address:</p>
                <p style="color: #008066;">${campaign.creatorWalletAddress}</p>
            </div>
            <div class="walletAddress">
                <p class="heading" style="color: #333;">Status:</p>
                <p style="color: #008066;">${campaign.status}</p>
            </div>
            <div class="walletAddress">
                <p class="heading" style="color: #333;">Target Amount:</p>
                <p style="color: #008066;">${campaign.targetAmount} XRP</p>
            </div>
            <div class="end-date">
                <p class="heading" style="color: #333;">End Date:</p>
                <p style="color: #008066;">${formattedEndDate}</p>
            </div>
            <div id="raisedAmount_${campaign.projectId}">
                <span class="raised-amount-text" style="color: #333;">Raised Amount: Loading...</span>
                <div class="progress-bar">
                    <div class="progress-bar-fill" style="width: 0%; background-color: #008066;">0%</div>
                </div>
            </div>
            <button class="view-transactions-btn" data-campaign-id="${campaign.projectId}">View Transactions</button>
        </div>
        <section class="projectDescription" data-aos="fade-up">
            <p>${campaign.description}</p>
        </section>
    </section>
`;


                // Attach event listener for viewing transactions
                const viewTransactionsBtn = campaignDetails.querySelector('.view-transactions-btn');
                viewTransactionsBtn.addEventListener('click', () => {
                    const projectId = viewTransactionsBtn.dataset.campaignId;
                    if (projectId) {
                        fetchCampaignTransactions(projectId);
                    } else {
                        console.error('Invalid campaign ID');
                    }
                });

                // Fetch raised amount for the campaign
                try {
                    const raisedAmountDiv = campaignDetails.querySelector(`#raisedAmount_${campaign.projectId}`);
                    const progressBarFill = raisedAmountDiv.querySelector('.progress-bar-fill');
                    const raisedAmountText = raisedAmountDiv.querySelector('.raised-amount-text');

                    const raisedAmountResponse = await fetch(`/projects/${campaign.projectId}/raised`);
                    const raisedAmountData = await raisedAmountResponse.json();

                    const raisedAmount = raisedAmountData.totalRaised;
                    const targetAmount = campaign.targetAmount;
                    const raisedPercentage = (raisedAmount / targetAmount) * 100;

                    progressBarFill.style.width = `${raisedPercentage}%`;
                    progressBarFill.textContent = `${raisedPercentage.toFixed(2)}%`;
                    raisedAmountText.innerHTML = `Raised Amount: <span class="value"> ${raisedAmount} XRP</span>`;
                } catch (error) {
                    console.error('Error fetching raised amount for campaign:', error);
                }
            };

            // Convert the ArrayBuffer to Blob
            const blob = new Blob([new Uint8Array(campaign.image.data)], { type: 'image/jpeg' });
            // Read the Blob as a data URL
            reader.readAsDataURL(blob);
        } else {
            console.error('Image data is missing for campaign:', campaign);
        }
    } catch (error) {
        console.error('Error fetching project details:', error);
        const campaignDetails = document.getElementById('campaignDetails');
        campaignDetails.innerHTML = '<p>Error loading project details. Please try again later.</p>'; // Show error message
    }
}

function getProjectIdFromUrl() {
    const urlParams = new URLSearchParams(window.location.search);
    return urlParams.get('projectId');
}

// Fetch project details when the page loads
window.onload = async () => {
    const projectId = getProjectIdFromUrl();
    if (projectId) {
        await fetchProjectDetails(projectId);
    } else {
        const campaignDetails = document.getElementById('campaignDetails');
        campaignDetails.innerHTML = '<p>Invalid project ID.</p>'; // Show error message
    }
};

function openOverlay(id) {
    const overlay = document.getElementById(id);
    overlay.style.display = 'block';
}

// Function to close overlay
function closeOverlay(id) {
    const overlay = document.getElementById(id);
    overlay.style.display = 'none';
}

async function fetchCampaignTransactions(projectId) {
    try {
        const response = await fetch(`/projects/${projectId}/transactions`);
        const transactions = await response.json();
        const transactionList = document.getElementById('transactionList');
        transactionList.innerHTML = '';

        if (transactions.length === 0) {
            const noTransactionsMessage = document.createElement('p');
            noTransactionsMessage.textContent = 'No transactions available.';
            transactionList.appendChild(noTransactionsMessage);
        } else {
            // Create the table and header row
            const table = document.createElement('table');
            table.classList.add('transaction-table');
            const headerRow = document.createElement('tr');
            headerRow.innerHTML = `
                <th>Ledger Index</th>
                <th>Transaction ID</th>
                <th>Sender</th>
                <th>Amount</th>
                <th>Time</th>
            `;
            table.appendChild(headerRow);

            // Reverse the order of transactions to display the latest ones at the top
            transactions.reverse().forEach(transaction => {
                const date = new Date(transaction.timestamp);
                const timeOptions = { hour: '2-digit', minute: '2-digit', second: '2-digit', hour12: true };
                const dateOptions = { year: 'numeric', month: 'long', day: 'numeric', timeZoneName: 'long' };
                const formattedTime = date.toLocaleTimeString('en-US', timeOptions);
                const formattedDate = date.toLocaleDateString('en-US', dateOptions);

                // Shorten the transaction ID, showing only the first 8 and last 8 characters
                const shortenedTransactionId = `${transaction.transactionId.slice(0, 8)}...${transaction.transactionId.slice(-8)}`;

                const row = document.createElement('tr');
                row.innerHTML = `
                    <td>${transaction.ledgerIndex}</td>
                    <td>
                        <span class="short-txid">${shortenedTransactionId}</span>
                        <span class="full-txid d-none">${transaction.transactionId}</span>
                        <button class="toggle-txid-btn" onclick="toggleTransactionId(this)">View Full</button>
                    </td>
                    <td>${transaction.sender}</td>
                    <td>${transaction.amount}</td>
                    <td>${formattedTime} on ${formattedDate}</td>
                `;
                table.appendChild(row);
            });

            transactionList.appendChild(table);
        }
        openOverlay('transactionOverlay'); // Show the overlay
    } catch (error) {
        console.error('Error fetching campaign transactions:', error);
    }
}

function toggleTransactionId(button) {
    const row = button.parentElement;
    const shortTxId = row.querySelector('.short-txid');
    const fullTxId = row.querySelector('.full-txid');

    if (fullTxId.classList.contains('d-none')) {
        // Show full transaction ID
        fullTxId.classList.remove('d-none');
        shortTxId.classList.add('d-none');
        button.textContent = 'View Short';
    } else {
        // Show short transaction ID
        fullTxId.classList.add('d-none');
        shortTxId.classList.remove('d-none');
        button.textContent = 'View Full';
    }
}

