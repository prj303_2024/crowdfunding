document.getElementById("createProjectForm").addEventListener("submit", async function (event) {
    event.preventDefault();

    const formData = new FormData(this);
    const endDate = formData.get('endDate'); 
    const loadingIndicator = document.getElementById('loadingProjectOverlay');

    // Check if the end date is in the past
    if (new Date(endDate) < new Date()) {
        Swal.fire({
            text: 'End date cannot be in the past. Please select a future date.',
            icon: 'error',
            confirmButtonText: 'OK'
        });
        return; 
    }

    try {
        // Show loading indicator before sending the request
        loadingIndicator.style.display = 'flex';

        const response = await fetch('/create-project', {
            method: 'POST',
            body: formData,
            credentials: 'same-origin'
        });

        if (!response.ok) {
            const errorText = await response.text();
            throw new Error(errorText || 'Failed to create project');
        }

        const data = await response.json();

        // Show success message
        Swal.fire({
            text: data.message,
            icon: 'success',
            confirmButtonText: 'OK'
        });

        // Optionally, you can reset the form after successful submission
        this.reset();

    } catch (error) {
        console.error(error);

        // Show error message
        Swal.fire({
            text: error.message || 'Error creating project',
            icon: 'error',
            confirmButtonText: 'Try Again'
        });
    } finally {
        // Hide loading indicator after the process finishes
        loadingIndicator.style.display = 'none';
    }
});
