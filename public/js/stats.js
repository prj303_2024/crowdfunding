fetch('/user-stats')
    .then(response => response.json())
    .then(data => {
        document.getElementById('totalUsers').textContent = data.totalUsers;
        document.getElementById('totalCreators').textContent = data.totalCreators;
        document.getElementById('totalInvestors').textContent = data.totalInvestors;
    })
    .catch(error => console.error('Error fetching user stats:', error));

// Fetch Project Stats
fetch('/project-stats')
    .then(response => response.json())
    .then(data => {
        document.getElementById('totalProjects').textContent = data.totalProjects;
        document.getElementById('totalActiveProjects').textContent = data.activeProjects;
        document.getElementById('totalCompletedProjects').textContent = data.completedProjects;
    })
    .catch(error => console.error('Error fetching project stats:', error));