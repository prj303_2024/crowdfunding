async function fetchCampaigns() {
    try {
        const campaignList = document.getElementById('campaignList');
        const loadingAnimation = document.querySelector('.loading-animation');

        // Ensure the loading animation is visible initially (if it is hidden by default)
        if (loadingAnimation) {
            loadingAnimation.style.display = 'flex';
        }

        const response = await fetch('/active-projects'); // Fetch all campaigns
        const campaigns = await response.json();

        // Hide the loading animation once campaigns are loaded
        if (loadingAnimation) {
            loadingAnimation.style.display = 'none';
        }

        if (campaigns.length === 0) {
            campaignList.innerHTML = '<p>No campaigns available.</p>'; // Show message if no campaigns
        } else {
            campaignList.innerHTML = ''; // Clear existing content
            // Reverse the order of campaigns to display the latest ones at the top
            campaigns.reverse().forEach(async (campaign) => {
                const campaignDiv = document.createElement('div');
                campaignDiv.classList.add('campaign-card');
                const reader = new FileReader();

                if (campaign.image && campaign.image.data) {
                    reader.onload = async function (event) {
                        const imageDataUrl = event.target.result;
                        const formattedEndDate = new Date(campaign.endDate).toLocaleDateString('en-US', {
                            year: 'numeric',
                            month: 'long',
                            day: 'numeric'
                        });

                        campaignDiv.innerHTML = `
                            <h3 class="project-title">${campaign.title}</h3>
                            <img src="${imageDataUrl}" alt="Campaign Image" width="350" height="200">
                            
                            <div class='amount-bar' id="raisedAmount_${campaign.projectId}">
                                <div class="progress-bar">
                                    <div class="progress-bar-fill" style="width: 0%;">0%</div>
                                </div>
                                <span class="raised-amount-text">Raised Amount: Loading...</span>
                            </div>
                            <p>Status: <span class='value'>${campaign.status}</span></p>
                            <p>Target Amount: <span class='value'>${campaign.targetAmount} XRP</span></p>
                            <p>End Date: <span class='value'>${formattedEndDate}</span></p>

                            <button class="view-details-btn" data-project-id="${campaign.projectId}">View Details</button>
                        `;
                        campaignList.appendChild(campaignDiv);

                        // Add event listener to the "View Details" button
                        const viewDetailsBtn = campaignDiv.querySelector('.view-details-btn');
                        viewDetailsBtn.addEventListener('click', () => {
                            const detailsPageUrl = '/creatorDetailsPage.html'; // Change this to your details page URL
                            window.location.href = detailsPageUrl;
                        });

                        const viewMoreBtn = campaignDiv.querySelector('.view-details-btn');
                        viewMoreBtn.addEventListener('click', () => {
                            const projectId = viewMoreBtn.dataset.projectId;
                            if (projectId) {
                                window.location.href = `../indexDetailsPage.html?projectId=${projectId}`;
                            } else {
                                console.error('Invalid project ID');
                            }
                        });

                        // Fetch raised amount for the campaign
                        try {
                            const raisedAmountDiv = document.querySelector(`#raisedAmount_${campaign.projectId}`);
                            const progressBarFill = raisedAmountDiv.querySelector('.progress-bar-fill');
                            const raisedAmountText = raisedAmountDiv.querySelector('.raised-amount-text');
                            const raisedAmountResponse = await fetch(`/projects/${campaign.projectId}/raised`);
                            const raisedAmountData = await raisedAmountResponse.json();
                            const raisedAmount = raisedAmountData.totalRaised;
                            const targetAmount = campaign.targetAmount;
                            const raisedPercentage = (raisedAmount / targetAmount) * 100;
                            progressBarFill.style.width = `${raisedPercentage}%`;
                            progressBarFill.textContent = `${raisedPercentage.toFixed(2)}%`;
                            raisedAmountText.innerHTML = `Raised Amount: <span class="raised-amount-value">${raisedAmount} XRP</span>`;
                        } catch (error) {
                            console.error('Error fetching raised amount for campaign:', error);
                        }
                    };

                    // Convert the ArrayBuffer to Blob
                    const blob = new Blob([new Uint8Array(campaign.image.data)], { type: 'image/jpeg' });

                    // Read the Blob as a data URL
                    reader.readAsDataURL(blob);
                } else {
                    console.error('Image data is missing for campaign:', campaign);
                }
            });
        }
    } catch (error) {
        console.error('Error fetching active campaigns:', error);
        const campaignList = document.getElementById('campaignList');
        campaignList.innerHTML = '<p>Error loading campaigns. Please try again later.</p>'; // Show error message
    }
}

// Fetch campaigns when the page loads
window.onload = async () => {
    await fetchCampaigns();
};
