document.addEventListener('DOMContentLoaded', () => {
    const loginForm = document.getElementById('loginForm');
    const errorMessage = document.querySelector('#loginModal .error-message');
    const successMessage = document.querySelector('#loginModal .success-message');
    let redirectUrl = ''; // Variable to store the redirect URL

    loginForm.addEventListener('submit', async (event) => {
        event.preventDefault(); // Prevent the default form submission behavior

        const formData = new FormData(loginForm); // Get form data
        const email = formData.get('email');
        const password = formData.get('password');

        try {
            const response = await fetch('/login', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ email, password }) // Send email and password as JSON
            });

            if (response.ok) {
                const userData = await response.json(); // Assuming server returns user data including type
                const userType = userData.userType;

                if (userType === 'investor') {
                    redirectUrl = '/home'; // Set redirect URL for investor
                } else if (userType === 'admin') {
                    redirectUrl = '/admin'; // Set redirect URL for admin
                } else if (userType === 'creator') {
                    redirectUrl = '/chome'; // Set redirect URL for creator
                } else {
                    showError('Unknown user type'); // Handle unknown user types
                    return; // Exit function to prevent further execution
                }

                // Show SweetAlert success message
                Swal.fire({
                    text: 'You have successfully logged in!',
                    icon: 'success',
                    confirmButtonText: 'done!'
                }).then(() => {
                    // Redirect to the specified URL after the alert is closed
                    window.location.href = redirectUrl;
                });

            } else {
                const errorText = await response.text();
                showError(errorText); // Show error with SweetAlert
            }
        } catch (error) {
            console.error('Error:', error);
            // Display a generic error message using SweetAlert
            showError('An error occurred. Please try again later.');
        }
    });

    function showError(message) {
        // Display the error message using SweetAlert
        Swal.fire({
            text: message,
            icon: 'error',
            confirmButtonText: 'OK'
        });
    }
});
