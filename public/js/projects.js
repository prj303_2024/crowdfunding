async function fetchCampaigns(searchQuery = '', status = '') {
    try {
        const campaignList = document.getElementById('campaignList');
        const loadingMessage = document.getElementById('loadingMessage');

        // Show loading message
        loadingMessage.style.display = 'block';

        // Fetch campaigns
        const response = await fetch('/active-projects');
        const campaigns = await response.json();

        // Hide loading message
        loadingMessage.style.display = 'none';

        // Filter campaigns based on search query and status
        const filteredCampaigns = campaigns.filter(campaign => {
            const matchesSearchQuery = campaign.title.toLowerCase().includes(searchQuery.toLowerCase()) ||
                campaign.description.toLowerCase().includes(searchQuery.toLowerCase());
            const matchesStatus = status === '' || campaign.status.toLowerCase() === status.toLowerCase();
            return matchesSearchQuery && matchesStatus;
        });

        if (filteredCampaigns.length === 0) {
            campaignList.innerHTML = '<p>No campaigns available.</p>'; // Show message if no campaigns
        } else {
            campaignList.innerHTML = ''; // Clear previous campaigns

            // Reverse the order of campaigns to display the latest ones at the top
            filteredCampaigns.reverse().forEach(async (campaign) => {
                const campaignDiv = document.createElement('div');
                const reader = new FileReader();

                if (campaign.image && campaign.image.data) {
                    reader.onload = async function (event) {
                        const imageDataUrl = event.target.result;
                        const formattedEndDate = new Date(campaign.endDate).toLocaleDateString('en-US', {
                            year: 'numeric',
                            month: 'long',
                            day: 'numeric'
                        });

                        campaignDiv.innerHTML = `
                            <div class="campaign-card">
                            <h3 class="project-title">${campaign.title}</h3>
                            <img src="${imageDataUrl}" alt="Campaign Image" width="350" height="200">
                            <div id="raisedAmount_${campaign.projectId}">
                                <div class="progress-bar">
                                    <div class="progress-bar-fill" style="width: 0%;">0%</div>
                                </div>

                                <div class="goal">
                                    <span class="raised-amount-text">
                                        <span class="label">Raised Amount:</span> 
                                        <span class="value">Loading...</span>
                                    </span>
                                </div>
                                <p class="target-amount">
                                        <b class="label">Target Amount:</b> 
                                        <span class="value">${campaign.targetAmount}</span>
                                </p>
                                <p>
                                    <b class="label">Status:</b> 
                                    <span class="value">${campaign.status}</span>
                                </p>
                                <p><b class="label">End Date: </b><span class='value'>${formattedEndDate}</span></p>
                            </div>
                            
                            <button class="view-details-btn" data-project-id="${campaign.projectId}">View Details</button>
                                
                            </div>
                        `;
                        campaignList.appendChild(campaignDiv);

                        // Attach event listener for "View More" button
                        const viewMoreBtn = campaignDiv.querySelector('.view-details-btn');
                        viewMoreBtn.addEventListener('click', () => {
                            const projectId = viewMoreBtn.dataset.projectId;
                            if (projectId) {
                                window.location.href = `../projectDetails.html?projectId=${projectId}`;
                            } else {
                                console.error('Invalid project ID');
                            }
                        });

                        // Fetch raised amount for the campaign
                        try {
                            const raisedAmountDiv = campaignDiv.querySelector(`#raisedAmount_${campaign.projectId}`);
                            const progressBarFill = raisedAmountDiv.querySelector('.progress-bar-fill');
                            const raisedAmountText = raisedAmountDiv.querySelector('.raised-amount-text');
                            const raisedAmountResponse = await fetch(`/projects/${campaign.projectId}/raised`);
                            const raisedAmountData = await raisedAmountResponse.json();
                            const raisedAmount = raisedAmountData.totalRaised;
                            const targetAmount = campaign.targetAmount;
                            const raisedPercentage = (raisedAmount / targetAmount) * 100;
                            progressBarFill.style.width = `${raisedPercentage}%`;
                            progressBarFill.textContent = `${raisedPercentage.toFixed(2)}%`;
                            raisedAmountText.innerHTML = `Raised Amount: <span class="value"> ${raisedAmount} XRP</span>`;
                        } catch (error) {
                            console.error('Error fetching raised amount for campaign:', error);
                        }
                    };

                    // Convert the ArrayBuffer to Blob
                    const blob = new Blob([new Uint8Array(campaign.image.data)], { type: 'image/jpeg' });

                    // Read the Blob as a data URL
                    reader.readAsDataURL(blob);
                } else {
                    console.error('Image data is missing for campaign:', campaign);
                }
            });
        }
    } catch (error) {
        console.error('Error fetching active campaigns:', error);
        const campaignList = document.getElementById('campaignList');
        campaignList.innerHTML = '<p>Error loading campaigns. Please try again later.</p>'; // Show error message
    }
}

// Add search functionality
document.getElementById('searchButton').addEventListener('click', async () => {
    const searchQuery = document.getElementById('searchInput').value;
    await fetchCampaigns(searchQuery);
});

// Add filter buttons functionality
document.getElementById('allButton').addEventListener('click', async () => {
    await fetchCampaigns();
});
document.getElementById('activeButton').addEventListener('click', async () => {
    await fetchCampaigns('', 'active');
});
document.getElementById('completedButton').addEventListener('click', async () => {
    await fetchCampaigns('', 'completed');
});
document.getElementById('endedButton').addEventListener('click', async () => {
    await fetchCampaigns('', 'ended');
});

// Fetch campaigns when the page loads
window.onload = async () => {
    await fetchCampaigns();
};
