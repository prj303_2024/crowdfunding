function flipElement(element) {
    var flipper = element.closest('.flipper');
    if (flipper.classList.contains('flipped')) {
        flipper.classList.remove('flipped');
    } else {
        flipper.classList.add('flipped');
    }
}
