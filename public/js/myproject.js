async function fetchCampaigns() {
    try {
        const loadingMessage = document.getElementById('loadingAnimation');
        const campaignList = document.getElementById('campaignList');

        // Show loading message
        loadingMessage.style.display = 'flex';

        const response = await fetch('/myprojects'); // Fetch all campaigns
        const campaigns = await response.json();

        // Hide loading message
        loadingMessage.style.display = 'none';

        if (campaigns.length === 0) {
            campaignList.innerHTML = '<p>No campaigns available.</p>'; // Show message if no campaigns
        } else {
            // Reverse the order of campaigns to display the latest ones at the top
            campaigns.reverse().forEach(async (campaign) => {
                const campaignDiv = document.createElement('div');
                campaignDiv.classList.add('campaign-card');
                const reader = new FileReader();

                if (campaign.image && campaign.image.data) {
                    reader.onload = async function (event) {
                        const imageDataUrl = event.target.result;

                        const formattedEndDate = new Date(campaign.endDate).toLocaleDateString('en-US', {
                            year: 'numeric',
                            month: 'long',
                            day: 'numeric'
                        });

                        campaignDiv.innerHTML = `
                            <h3 class="project-title">${campaign.title}</h3>
                            <img src="${imageDataUrl}" alt="Campaign Image" width="350" height="200">
                            
                            <div class='amount-bar' id="raisedAmount_${campaign.projectId}">
                                <div class="progress-bar">
                                    <div class="progress-bar-fill" style="width: 0%;">0%</div>
                                </div>
                                <span class="raised-amount-text">Raised Amount: Loading...</span>
                            </div>
                            <p>Target Amount: <span class='value'>${campaign.targetAmount} XRP</span></p>
                            <p>Status: <span class='value'>${campaign.status}</span></p>
                            <p>End Date: <span class='value'>${formattedEndDate}</span></p>

                            <button class="view-details-btn" data-project-id="${campaign.projectId}">View Details</button>
                        `;
                        campaignList.appendChild(campaignDiv);

                        // Add event listener to the "View Details" button
                        const viewDetailsBtn = campaignDiv.querySelector('.view-details-btn');
                        viewDetailsBtn.addEventListener('click', () => {
                            const projectId = viewDetailsBtn.dataset.projectId;

                            if (projectId) {
                                window.location.href = `../creatorDetailsPage.html?projectId=${projectId}`;
                            } else {
                                console.error('Invalid project ID');
                            }
                        });

                        // Fetch raised amount for the campaign
                        try {
                            const raisedAmountDiv = document.querySelector(`#raisedAmount_${campaign.projectId}`);
                            const progressBarFill = raisedAmountDiv.querySelector('.progress-bar-fill');
                            const raisedAmountText = raisedAmountDiv.querySelector('.raised-amount-text');
                            const raisedAmountResponse = await fetch(`/projects/${campaign.projectId}/raised`);
                            const raisedAmountData = await raisedAmountResponse.json();
                            const raisedAmount = raisedAmountData.totalRaised;
                            const targetAmount = campaign.targetAmount;
                            const raisedPercentage = (raisedAmount / targetAmount) * 100;
                            progressBarFill.style.width = `${raisedPercentage}%`;
                            progressBarFill.textContent = `${raisedPercentage.toFixed(2)}%`;
                            raisedAmountText.innerHTML = `Raised Amount: <span class="raised-amount-value">${raisedAmount} XRP</span>`;
                        } catch (error) {
                            console.error('Error fetching raised amount for campaign:', error);
                        }
                    };

                    // Convert the ArrayBuffer to Blob
                    const blob = new Blob([new Uint8Array(campaign.image.data)], { type: 'image/jpeg' });

                    // Read the Blob as a data URL
                    reader.readAsDataURL(blob);
                } else {
                    console.error('Image data is missing for campaign:', campaign);
                }
            });
        }
    } catch (error) {
        console.error('Error fetching active campaigns:', error);
        const campaignList = document.getElementById('campaignList');
        campaignList.innerHTML = '<p>Error loading campaigns. Please try again later.</p>'; // Show error message
    }
}

// Fetch campaigns when the page loads
window.onload = async () => {
    // You can call a loading function here if needed
    await fetchCampaigns();
};


function openOverlay(id) {
    const overlay = document.getElementById(id);
    overlay.style.display = 'block';
}

// Function to close overlay
function closeOverlay(id) {
    const overlay = document.getElementById(id);
    overlay.style.display = 'none';
}

async function fetchCampaignTransactions(projectId) {
    try {
        const response = await fetch(`/projects/${projectId}/transactions`);
        const transactions = await response.json();
        const transactionList = document.getElementById('transactionList');
        transactionList.innerHTML = '';

        if (transactions.length === 0) {
            const noTransactionsMessage = document.createElement('p');
            noTransactionsMessage.textContent = 'No transactions available.';
            transactionList.appendChild(noTransactionsMessage);
        } else {
            // Create the table and header row
            const table = document.createElement('table');
            table.classList.add('transaction-table');
            const headerRow = document.createElement('tr');
            headerRow.innerHTML = `
                <th>Transaction ID</th>
                <th>Sender</th>
                <th>Receiver</th>
                <th>Amount</th>
                <th>Time</th>
            `;
            table.appendChild(headerRow);

            // Reverse the order of transactions to display the latest ones at the top
            transactions.reverse().forEach(transaction => {
                const date = new Date(transaction.timestamp);
                const timeOptions = { hour: '2-digit', minute: '2-digit', second: '2-digit', hour12: true };
                const dateOptions = { year: 'numeric', month: 'long', day: 'numeric', timeZoneName: 'long' };
                const formattedTime = date.toLocaleTimeString('en-US', timeOptions);
                const formattedDate = date.toLocaleDateString('en-US', dateOptions);

                const row = document.createElement('tr');
                row.innerHTML = `
                    <td>${transaction.transactionId}</td>
                    <td>${transaction.sender}</td>
                    <td>${transaction.receiver}</td>
                    <td>${transaction.amount}</td>
                    <td>${formattedTime} on ${formattedDate}</td>
                `;
                table.appendChild(row);
            });

            transactionList.appendChild(table);
        }
        openOverlay('transactionOverlay'); // Show the overlay
    } catch (error) {
        console.error('Error fetching campaign transactions:', error);
    }
}


// Update the initiateInvestment function
async function initiateInvestment(projectId) {
    // Show the investment modal
    document.getElementById('investmentModal').style.display = 'block';

    // Get the modal and the form elements
    const modal = document.getElementById('investmentModal');
    const form = document.getElementById('investmentForm');
    const walletSecretInput = document.getElementById('walletSecret');
    const amountInput = document.getElementById('amount');

    // Add event listener for form submission
    form.addEventListener('submit', async (event) => {
        event.preventDefault();

        const walletSecret = walletSecretInput.value;
        const amount = amountInput.value;

        if (!walletSecret || !amount) {
            alert('Please provide both wallet secret and investment amount.');
            return;
        }

        // Send the investment request
        const response = await fetch('/invest', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ walletSecret, amount, projectId }),
        });

        const data = await response.json();

        if (response.ok) {
            alert('Investment successful!');
            console.log('Balance changes:', data.balanceChanges);
        } else {
            alert(`Investment failed: ${data.error}`);
        }

        // Close the modal
        modal.style.display = 'none';
    });

    // Add event listener for closing the modal
    window.addEventListener('click', (event) => {
        if (event.target === modal) {
            modal.style.display = 'none';
        }
    });

    // Add event listener for closing the modal when the close button is clicked
    const closeBtn = document.getElementsByClassName('close-invest')[0];
    closeBtn.addEventListener('click', () => {
        modal.style.display = 'none';
    });
}
