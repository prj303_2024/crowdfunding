async function loadProjects() {
    const loadingAnimation = document.getElementById('loadingAnimation');
    const campaignList = document.getElementById('projectList');

    try {
        // Show loading animation
        loadingAnimation.style.display = 'flex';

        const response = await fetch('/active-projects'); // Fetch all campaigns
        const campaigns = await response.json();

        if (campaigns.length === 0) {
            campaignList.innerHTML = '<p>No campaigns available.</p>'; // Show message if no campaigns
        } else {
            campaignList.innerHTML = ''; // Clear previous content

            // Reverse the order of campaigns to display the latest ones at the top
            const limitedCampaigns = campaigns.reverse().slice(0, 3);

            limitedCampaigns.forEach(async (campaign) => {
                const campaignDiv = document.createElement('div');
                campaignDiv.classList.add('project-card');
                const reader = new FileReader();

                if (campaign.image && campaign.image.data) {
                    reader.onload = async function (event) {
                        const imageDataUrl = event.target.result;

                        campaignDiv.innerHTML = `
    <div class="campaign-card">
        <h3 class="project-title">${campaign.title}</h3>
        <div class="image-container">
            <img src="${imageDataUrl}" alt="Campaign Image" width="350" height="200">
        </div>
        <div id="raisedAmount_${campaign.projectId}">
            <div class="progress-bar">
                <div class="progress-bar-fill" style="width: 0%;">0%</div>
            </div>
            <div class="goal">
                <span class="raised-amount-text">Raised Amount: Loading...</span>
                <p>Target Amount: <span class='value'>${campaign.targetAmount} XRP</span></p>
            </div>
        </div>
        <p id="status-index">
            <b>Status: </b><span id="campaign-status">${campaign.status}</span>
        </p>
    </div>
`;

                        campaignList.appendChild(campaignDiv);

                        // Fetch raised amount for the campaign
                        try {
                            const raisedAmountDiv = document.querySelector(`#raisedAmount_${campaign.projectId}`);
                            const progressBarFill = raisedAmountDiv.querySelector('.progress-bar-fill');
                            const raisedAmountText = raisedAmountDiv.querySelector('.raised-amount-text');
                            const raisedAmountResponse = await fetch(`/projects/${campaign.projectId}/raised`);
                            const raisedAmountData = await raisedAmountResponse.json();
                            const raisedAmount = raisedAmountData.totalRaised;
                            const targetAmount = campaign.targetAmount;
                            const raisedPercentage = (raisedAmount / targetAmount) * 100;
                            progressBarFill.style.width = `${raisedPercentage}%`;
                            progressBarFill.textContent = `${raisedPercentage.toFixed(2)}%`;
                            raisedAmountText.innerHTML = `Raised Amount: <span class="raised-amount-value">${raisedAmount} XRP</span>`;
                        } catch (error) {
                            console.error('Error fetching raised amount for campaign:', error);
                        }
                    };

                    // Convert the ArrayBuffer to Blob
                    const blob = new Blob([new Uint8Array(campaign.image.data)], { type: 'image/jpeg' });

                    // Read the Blob as a data URL
                    reader.readAsDataURL(blob);
                } else {
                    console.error('Image data is missing for campaign:', campaign);
                }
            });
        }
    } catch (error) {
        console.error('Error fetching active campaigns:', error);
        campaignList.innerHTML = '<p>Error loading campaigns. Please try again later.</p>'; // Show error message
    } finally {
        // Hide loading animation
        loadingAnimation.style.display = 'none';
    }
}

async function fetchAndDisplayFeedback() {
    try {
        // Fetch feedback data from the server
        const response = await fetch('/feedback');
        const feedbackData = await response.json();

        // Get the container element where feedback will be displayed
        const feedbackContainer = document.getElementById('feedback-container');

        // Counter to keep track of displayed feedbacks
        let feedbackCount = 0;

        // Loop through each feedback entry and create a card for it
        feedbackData.forEach(feedback => {
            if (feedbackCount >= 3) return; // Exit loop if 3 feedbacks are displayed

            // Create a div element for the feedback card
            const feedbackCard = document.createElement('div');
            feedbackCard.classList.add('feedback-card');

            // Create rating section for the feedback rating
            const rating = document.createElement('div');
            rating.classList.add('rating');
            rating.innerHTML = `<span>Rating: </span>`;
            for (let i = 0; i < feedback.rating; i++) {
                const starIcon = document.createElement('img');
                starIcon.src = '../img/Star.png'; // Assuming you have a star icon image
                rating.appendChild(starIcon);
            }
            feedbackCard.appendChild(rating);

            // Create div for border above the feedback message
            const borderTop = document.createElement('div');
            borderTop.classList.add('border-top');
            feedbackCard.appendChild(borderTop);

            // Create message section for the feedback message
            const message = document.createElement('div');
            message.classList.add('feedback-message');
            message.textContent = feedback.message;
            feedbackCard.appendChild(message);

            // Create div for border below the feedback message
            const borderBottom = document.createElement('div');
            borderBottom.classList.add('border-bottom');
            feedbackCard.appendChild(borderBottom);

            // Create info section for name, email, and date
            const info = document.createElement('div');
            info.classList.add('feedback-info');
            info.innerHTML = `${feedback.name}<br>`;
            feedbackCard.appendChild(info);

            // Append the feedback card to the container
            feedbackContainer.appendChild(feedbackCard);

            feedbackCount++; // Increment the feedback counter
        });
    } catch (error) {
        console.error('Error fetching feedback:', error);
    }
}

// Combine both functions in a single window.onload handler
window.onload = async () => {
    await loadProjects();
    await fetchAndDisplayFeedback();
};
