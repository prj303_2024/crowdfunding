document.addEventListener('DOMContentLoaded', async () => {
    const walletBalanceDiv = document.getElementById('wallet-balance');

    // Display loading message
    walletBalanceDiv.textContent = 'Loading wallet balance...';

    try {
        const response = await fetch('/wallet-balance');
        const data = await response.json();

        if (data.success) {
            walletBalanceDiv.textContent = `Wallet Balance: ${data.balance} XRP`;
        } else {
            walletBalanceDiv.textContent = 'Failed to fetch wallet balance.';
        }
    } catch (error) {
        console.error('Error fetching wallet balance:', error);
        walletBalanceDiv.textContent = 'An error occurred while fetching the wallet balance.';
    }
});
