async function fetchAndDisplayFeedback() {
    try {
        // Fetch feedback data from the server
        const response = await fetch('/feedback');
        const feedbackData = await response.json();

        // Get the container element where feedback will be displayed
        const feedbackContainer = document.getElementById('feedback-container');

        // Counter to keep track of displayed feedbacks
        let feedbackCount = 0;

        // Loop through each feedback entry and create a card for it
        feedbackData.forEach(feedback => {
            if (feedbackCount >= 3) return; // Exit loop if 3 feedbacks are displayed

            // Create a div element for the feedback card
            const feedbackCard = document.createElement('div');
            feedbackCard.classList.add('feedback-card');

            // Create rating section for the feedback rating
            const rating = document.createElement('div');
            rating.classList.add('rating');
            rating.innerHTML = `<span>Rating: </span>`;
            for (let i = 0; i < feedback.rating; i++) {
                const starIcon = document.createElement('img');
                starIcon.src = '../img/Star.png'; // Assuming you have a star icon image
                rating.appendChild(starIcon);
            }
            feedbackCard.appendChild(rating);

            // Create div for border above the feedback message
            const borderTop = document.createElement('div');
            borderTop.classList.add('border-top');
            feedbackCard.appendChild(borderTop);

            // Create message section for the feedback message
            const message = document.createElement('div');
            message.classList.add('feedback-message');
            message.textContent = feedback.message;
            feedbackCard.appendChild(message);

            // Create div for border below the feedback message
            const borderBottom = document.createElement('div');
            borderBottom.classList.add('border-bottom');
            feedbackCard.appendChild(borderBottom);

            // Create info section for name, email, and date
            const info = document.createElement('div');
            info.classList.add('feedback-info');
            info.innerHTML = `${feedback.name}<br>`;
            feedbackCard.appendChild(info);

            // Append the feedback card to the container
            feedbackContainer.appendChild(feedbackCard);

            feedbackCount++; // Increment the feedback counter
        });
    } catch (error) {
        console.error('Error fetching feedback:', error);
    }
}

// Call the function to fetch and display feedback when the page loads
window.onload = fetchAndDisplayFeedback;