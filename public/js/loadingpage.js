document.addEventListener("DOMContentLoaded", function() {
    // Show loading overlay
    const loadingOverlay = document.getElementById('loadingOverlay');
    loadingOverlay.style.display = 'flex'; // Make the loading overlay visible

    // Hide loading overlay after 3 seconds
    setTimeout(function() {
        loadingOverlay.style.display = 'none'; // Hide the loading overlay
    }, 1600);
});
