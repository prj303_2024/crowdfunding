document.addEventListener("DOMContentLoaded", async function () {
    try {
        const response = await fetch('/profile');
        const data = await response.json();

        if (response.ok) {
            displayProfile(data);
        } else {
            throw new Error(data.message || 'Failed to fetch user profile');
        }
    } catch (error) {
        console.error('Error fetching user profile:', error);
        displayError(error.message || 'Failed to fetch user profile');
    }
});

function displayProfile(profile) {
    const profileInfoContainer = document.getElementById('profile-info');
    profileInfoContainer.innerHTML = `
    ${profile.profilePicture ? `<div class="profile-picture"><div class="pp-div"></div><img src="${profile.profilePicture}" alt="Profile Picture"></div>` : '<div class="profile-picture"></div>'}
        <div class="details-and-actions">
            <div class="details">
                <p><strong>Name:</strong> ${profile.name}</p>
                <p><strong>Email:</strong> ${profile.email}</p>
                <p><strong>Wallet Address:</strong> <br/> ${profile.walletAddress}</p>
                <p><strong>User Type:</strong> ${profile.userType}</p>
            </div>
            <div id="profile-actions">
                <a href="profileupdate.html">Edit Profile</a>
                <a href="mytransaction.html">My Transactions</a>
            </div>
        </div>
    `;
}



function displayError(message) {
    const profileInfoContainer = document.getElementById('profile-info');
    profileInfoContainer.innerHTML = `<p>${message}</p>`;
}