
document.addEventListener('DOMContentLoaded', async () => {
    try {
        const response = await fetch('/profile');
        if (!response.ok) {
            throw new Error('Failed to fetch profile information');
        }
        const userProfile = await response.json();

        // Update the profile image src with the user's profile picture URL
        const profileImg = document.getElementById('profile-img');
        if (userProfile.profilePicture) {
            profileImg.src = userProfile.profilePicture;
        } else {
            profileImg.src = 'img/chador.png'; // Default image if no profile picture is found
        }
    } catch (error) {
        console.error('Error retrieving user profile:', error);
    }
});

