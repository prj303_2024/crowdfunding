document.addEventListener('DOMContentLoaded', () => {
    const logoutLink = document.getElementById('logoutLink');

    logoutLink.addEventListener('click', async (event) => {
        event.preventDefault(); // Prevent the default action (navigating away)

        // Show the SweetAlert confirmation dialog
        const result = await Swal.fire({
            title: 'Are you sure?',
            text: 'Do you really want to log out?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, log me out!',
            cancelButtonText: 'Cancel',
            customClass: {
                confirmButton: 'confirm-logout-btn',
                cancelButton: 'cancel-logout-btn'
            }
        });

        // If the user confirms, proceed with the logout
        if (result.isConfirmed) {
            try {
                const response = await fetch('/logout', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                });

                if (response.ok) {
                    // Show success message using SweetAlert
                    Swal.fire({
                        icon: 'success',
                        text: 'Logout successful!',
                        confirmButtonText: 'OK'
                    }).then(() => {
                        // Redirect the user to the landing page after logout
                        window.location.href = '/';
                    });
                } else {
                    // Show error message using SweetAlert
                    Swal.fire({
                        icon: 'error',
                        text: 'Logout failed, please try again.'
                    });
                }
            } catch (error) {
                // Show generic error message
                Swal.fire({
                    icon: 'error',
                    text: 'Error logging out, please try again later.'
                });
                console.error('Error during logout:', error);
            }
        }
    });
});
